(require "asdf")

; The tests are marked “superficial” in debian/tests/control, since this only
; tries to load the system

(let ((asdf:*user-cache* (uiop:getenv "AUTOPKGTEST_TMP"))) ; Store FASL in some temporary dir
  (asdf:load-system "abnf"))
